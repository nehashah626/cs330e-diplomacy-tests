#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    '''
    def test_read_1(self):
        s = "A Madrid Hold\n"
        l = diplomacy_read(s)
        self.assertEqual(l, ['A', 'Madrid', 'Hold'])

    
    def test_read_2(self):
        s = "C Austin Move London\n"
        l = diplomacy_read(s)
        self.assertEqual(l, ['C', 'Austin', 'Move', 'London'])

    def test_read_3(self):
        s = "A Madrid Support C\n"
        l = diplomacy_read(s)
        self.assertEqual(l, ['A', 'Madrid', 'Support', 'C'])
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move', 'Madrid'],['C', 'London', 'Support', 'B']])
        self.assertEqual(v,[['A','[dead]'],['B', 'Madrid'],['C', 'London']])

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move', 'Madrid'],['C', 'London', 'Move', 'Madrid'],['D', 'Paris', 'Support', 'B'],['E', 'Austin', 'Support', 'A']])
        self.assertEqual(v, [['A', '[dead]'],['B', '[dead]'],['C', '[dead]'],['D', 'Paris'],['E', 'Austin']])
    
    def test_eval_3(self):
        v = diplomacy_eval("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")
    '''
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Hold\nC London Support A\nD Austin Support C\nE Boston Move Houston\nF Houston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Madrid\nB [dead]\nC London\nD [dead]\nE Houston\nF [dead]\n')
    
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
