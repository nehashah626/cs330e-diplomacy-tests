#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, diplomacy_print

# -----------
# TestCollatz
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold\n")
        l = diplomacy_read(s)
        self.assertEqual(l, [{'A': 'Madrid'}, {}])

    def test_read_2(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        l = diplomacy_read(s)
        self.assertEqual(l, [{'A': 'Madrid', 'B': 'Madrid'}, {}])

    def test_read_3(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        l = diplomacy_read(s)
        self.assertEqual(l, [{'A': 'Madrid', 'B': 'Madrid', 'C': 'London'}, {'B': 'C'}])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        s = [{'A': 'Madrid'}, {}]
        result = diplomacy_eval(s)
        self.assertEqual(result, ['A Madrid'])

    def test_eval_2(self):
        s = [{'A': 'Madrid', 'B': 'Madrid'}, {}]
        result = diplomacy_eval(s)
        self.assertEqual(result, ['A [dead]', 'B [dead]'])

    def test_eval_3(self):
        s = [{'A': 'Madrid', 'B': 'Madrid', 'C': 'London'}, {'B': 'C'}]
        result = diplomacy_eval(s)
        self.assertEqual(result, ['A [dead]', 'B Madrid', 'C London'])

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Support C\nB Barcelona Hold\nC London Hold\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Austin Support A\nE NYC Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Austin\nE London\n")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C London"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
