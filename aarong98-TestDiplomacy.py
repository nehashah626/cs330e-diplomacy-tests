from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    # first 5 cases that are given in project description

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n") 

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n") 

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n") 

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # no input corner case
    def test_solve_8(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "\n")

    # corner case where two army attack one holding army while one army attacks one of the other two cities
    def test_solve_9(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Move Madrid\nD Houston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Austin\n")

    # corner case where multiple armies support one army and other army attacks one of those cities that had an army leave 
    def test_solve_10(self):
        r = StringIO("A NewYork Hold\nB London Support A\nC Austin Support A\nD Houston Move NewYork\nE SanFrancisco Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A NewYork\nB London\nC Austin\nD [dead]\nE SanFrancisco\n")

    # corner case 
    def test_solve_11(self):
        r = StringIO("A NewYork Hold\nB London Support A\nC Austin Support A\nD Houston Move Austin\nE SanFrancisco Support D\nF LosAngeles Move NewYork\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A NewYork\nB London\nC [dead]\nD Austin\nE SanFrancisco\nF [dead]\n")

    def test_solve_12(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Austin Move London\nD Beijing Move Madrid\nE Dallas Support C\nF NewYork Support C\nG LosAngeles Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Dallas\nF NewYork\nG LosAngeles\n")


    
if __name__ == "__main__": #pragma: no cover


    
    main()

