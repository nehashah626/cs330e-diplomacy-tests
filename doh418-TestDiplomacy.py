
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n ")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO()
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

    def test_solve_3(self):
        r = StringIO("E Austin Support A\nC London Move Madrid\nA Madrid Hold\nB Barcelona Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" # pragma: no cover
...
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          63      1     32      2    97%   14->19, 75->76, 76
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                 84      1     32      2    97%

"""
